public class Account {

	private int accountNumber;

	public Account(int accountNumber) {

		// Constructor

		this.accountNumber = accountNumber;

	}

	public int getAccountNumber(){
 		// return the account number
		return accountNumber;

	}

	public List<Transaction> getTransactions()
	{

		try {

			//Get the list of transactions

			List dbTransactionList = Db.getTransactions(accountNumber);
			List transactionList = Db.getTransactions(accountNumber);
			
			for (Iterator i = dbTransactionList.iterator(); i.hasNext()) {

				DbRow dbRow = (DbRow) dbTransactionList.get(i);

				Transaction trans = makeTransactionFromDbRow(dbRow);

				transactionList.add(trans);

			}

			return transactionList;

		} catch {

			// There was a database error

			return new List<Transaction>();
		}

	}

	private Transaction makeTransactionFromDbRow(DbRow row) {

		double currencyAmountInPounds = Double.parseDouble(row.getValueForField("amt"));

		String description = row.getValueForField("desc");
		// return the new Transaction object
		return new Transaction(description, currencyAmountInPounds);

	}

	// Override the equals method

	public boolean equals(Account o) {

		// check account numbers are the same

		return o.getAccountNumber() == getAccountNumber();

	}

}